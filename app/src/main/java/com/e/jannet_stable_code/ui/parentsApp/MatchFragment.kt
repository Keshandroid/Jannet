package com.e.jannet_stable_code.ui.parentsApp

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.e.jannet_stable_code.R
import com.e.jannet_stable_code.adapter.EventBookMatchListAdapter
import com.e.jannet_stable_code.adapter.MatchTabListAdapter
import com.e.jannet_stable_code.retrofit.ControllerInterface
import com.e.jannet_stable_code.retrofit.controller.EventListController
import com.e.jannet_stable_code.retrofit.controller.GetParentMatchController
import com.e.jannet_stable_code.retrofit.matchlistdata.MatchListBaseResponse
import com.e.jannet_stable_code.retrofit.matchlistdata.MatchListResult
import com.e.jannet_stable_code.retrofit.response.EventListResponse
import com.e.jannet_stable_code.utils.Utilities.showToast
import kotlinx.android.synthetic.main.activity_booke_event.*
import kotlinx.android.synthetic.main.fragment_match.*
import kotlinx.android.synthetic.main.topbar_layout.*

class MatchFragment : Fragment(R.layout.fragment_match)
    {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTopBar()
   }

    private fun openEventDetails() {
        startActivity(Intent(requireActivity(),EventDetailsActivity::class.java).putExtra("from","match"))
    }

    override fun onResume() {
        super.onResume()

        GetParentMatchController(requireActivity(), object : ControllerInterface,
            MatchTabListAdapter.IMatchItemClickClickListner {
            override fun onFail(error: String?) {

            }
            override fun <T> onSuccess(response: T, method: String) {
                try {

                    val resp = response as  List<MatchListResult?>?
                    var adapter = MatchTabListAdapter(requireContext(), resp)
                    rcvMatchList.adapter = adapter
                    adapter.iMatchItemClickClickListner = this
                    adapter.notifyDataSetChanged()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onClick(result: MatchListResult) {
              //  var list:List<MatchListResult?>?=listOf<MatchListResult>()
                val intent = Intent (getActivity(), RegisteredMatchActivity::class.java)
                intent.putExtra("getTeamAName",result.getTeamAName())
                intent.putExtra("getTeamAImage",result.getTeamAImage())
                intent.putExtra("getTeamBName",result.getTeamBName())
                intent.putExtra("getTeamBImage",result.getTeamBImage())
                intent.putExtra("date",result.getDate()+result.getTime())
                getActivity()?.startActivity(intent)


            }
        }).callApi()
    }

    private fun setTopBar() {
        imgBack.visibility=View.VISIBLE
        imgHistory.visibility=View.VISIBLE
        imgBack.setOnClickListener { (requireActivity() as ParentsMainActivity).onBackPressed() }
        txtTitle.text=getString(R.string.match1)
    }

}

private fun Parcelable.putExtra(s: String, result: MatchListResult) {

}
