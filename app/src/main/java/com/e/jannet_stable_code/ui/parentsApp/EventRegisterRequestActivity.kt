package com.e.jannet_stable_code.ui.parentsApp

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.e.jannet_stable_code.R
import kotlinx.android.synthetic.main.activity_event_register_request.*
import kotlinx.android.synthetic.main.topbar_layout.*

class EventRegisterRequestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_register_request)

        init()
    }

    private fun init() {
        imgBack.isVisible = true
        txtTitle.text = "Event Register Request"
        txtTitle.isVisible = true

        imgBack.setOnClickListener {

            onBackPressed()
            finish()
        }
        val spinnerEventRequest = findViewById<Spinner>(R.id.spinner_event_request)

        txt_event_request.setOnClickListener {

            spinnerEventRequest.performClick()
        }
        val arrayEventRequest = resources.getStringArray(R.array.TicketRequest)

        if (spinnerEventRequest != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, arrayEventRequest
            )
            spinnerEventRequest.adapter = adapter
        }

        spinnerEventRequest.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {


            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {


                txt_event_request.text = arrayEventRequest[position]

                if (arrayEventRequest[position]=="Current Request"){

                    cv_event_register_request_1.isVisible =true
                    cv_event_register_request_2.isGone =true
                    cv_event_register_request_3.isGone =true
                }else if (arrayEventRequest[position]=="Accpted Request"){


                    cv_event_register_request_1.isGone =true
                    cv_event_register_request_2.isVisible =true
                    cv_event_register_request_3.isGone =true

                }else if (arrayEventRequest[position]=="Rejected Request"){


                    cv_event_register_request_1.isGone =true
                    cv_event_register_request_2.isGone =true
                    cv_event_register_request_3.isVisible =true
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }

    }
}

