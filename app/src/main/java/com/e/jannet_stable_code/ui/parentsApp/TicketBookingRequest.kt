package com.e.jannet_stable_code.ui.parentsApp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.isGone
import androidx.core.view.isVisible
import com.e.jannet_stable_code.R
import kotlinx.android.synthetic.main.activity_add_match_f.*
import kotlinx.android.synthetic.main.activity_ticket_booking_request.*
import kotlinx.android.synthetic.main.topbar_layout.*

class TicketBookingRequest : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_booking_request)

        init()
    }

    private fun init() {
        imgBack.isVisible = true
        txtTitle.text = "Ticket Booking Request"
        txtTitle.isVisible = true
        val spinnerTicketRequest = findViewById<Spinner>(R.id.spinner_ticket_request)

        imgBack.setOnClickListener {

            onBackPressed()
            finish()
        }

        txt_tiket_request.setOnClickListener {
            spinner_ticket_request.performClick()
        }
        val arrayTicketRequest = resources.getStringArray(R.array.TicketRequest)
//            ArrayAdapter(this, android.R.layout.simple_spinner_item,arrayTicketRequest)
//        arrayTicketRequest.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//        spinner_ticket_request.adapter = arrayTicketRequest

        if (spinnerTicketRequest != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, arrayTicketRequest
            )
            spinnerTicketRequest.adapter = adapter
        }

        spinnerTicketRequest.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {


            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                Toast.makeText(
                    this@TicketBookingRequest, getString(R.string.selected_item) + " " +
                            "" + arrayTicketRequest[position], Toast.LENGTH_SHORT
                ).show()

                txt_tiket_request.text = arrayTicketRequest[position]

                if (arrayTicketRequest[position]=="Current Request"){

                    cv_ticket_request_1.isVisible =true
                    cv_ticket_request_2.isGone =true
                    cv_ticket_request_3.isGone =true
                }else if (arrayTicketRequest[position]=="Accpted Request"){


                    cv_ticket_request_1.isGone =true
                    cv_ticket_request_2.isVisible =true
                    cv_ticket_request_3.isGone =true

                }else if (arrayTicketRequest[position]=="Rejected Request"){


                    cv_ticket_request_1.isGone =true
                    cv_ticket_request_2.isGone =true
                    cv_ticket_request_3.isVisible =true
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }


    }
}


