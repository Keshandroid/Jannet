package com.e.jannet_stable_code.ui.parentsApp

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.core.view.drawToBitmap
import com.e.jannet_stable_code.R
import com.e.jannet_stable_code.canvasLib.CanvasView
import com.e.jannet_stable_code.retrofit.controller.BookChildEventController
import com.e.jannet_stable_code.retrofit.controller.IBaseController
import com.e.jannet_stable_code.retrofit.controller.JoinTeamFromParentController
import com.e.jannet_stable_code.ui.BaseActivity
import com.e.jannet_stable_code.utils.ImageFilePath
import com.e.jannet_stable_code.viewinterface.RegisterControllerInterface
import kotlinx.android.synthetic.main.activity_book_signature.*
import kotlinx.android.synthetic.main.topbar_layout.*
import java.io.ByteArrayOutputStream


class BookSignatureActivity : BaseActivity(), RegisterControllerInterface {
    var canvasView: CanvasView? = null
    lateinit var controller: BookChildEventController
    private var imageUri: Uri? = null
    lateinit var joinTeamController: JoinTeamFromParentController


    override fun getController(): IBaseController? {
        return null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_signature)

        var eventID = intent.getStringExtra("eventId")

        setTopBar()
        controller = BookChildEventController(this, this)
        joinTeamController = JoinTeamFromParentController(this, this)
        imgPrivacy.setOnClickListener {
            privacyClicked()
        }
        txtPrivacy.setOnClickListener { imgPrivacy.performClick() }
        txtClear.setOnClickListener { canvasView!!.clearCanvas() }

        canvasView = CanvasView(this)
        rlSign.addView(canvasView)
//        Log.e("TAG", "onCreate:booksignature  ${rlSign.drawToBitmap()}")

        val coachId = intent.getStringExtra("Coach_id")
        val fees = intent.getStringExtra("fees")
        val team_id = intent.getStringExtra("Team_id")
        val parentJoin = intent.getStringExtra("COACH_JOIN")
        txtBook.setOnClickListener {

            if (parentJoin?.trim().toString() == "coach_join") {
                imageUri = getImageUri(getBitmapFromView(rlSign)!!)
                //book team from parent side

                var registerData = RegisterData()
                registerData.coach_id = coachId?.trim().toString()
                registerData.teamId = team_id?.trim().toString()
                registerData.fees = fees?.trim().toString()
                registerData.image = ImageFilePath.getPath(this, imageUri!!)!!



                joinTeamController.joinTeam(registerData)

            } else {
                imageUri = getImageUri(getBitmapFromView(rlSign)!!)


                val eventid = intent.getStringExtra("eventId")
                val childId = intent.getStringExtra("ChildId")
                val fess = intent.getStringExtra("Fees")
                Log.e("TAG", "onCreate:booksignature  ${fess + childId + eventid}")
//            showToast("Book Successful")
//            val intent =Intent(this,EventDetailsActivity::class.java)
//            intent.putExtra("eventId",eventID)
//            startActivity(intent)
                var registerData = RegisterData()
                registerData.child_id = childId.toString()
                registerData.fees = fess.toString()
                registerData.event_id = eventid.toString()
                registerData.image = ImageFilePath.getPath(this, imageUri!!)!!

                controller.bookEvent(registerData)
                Log.e("TAG", "onCreate:booksignature  ${ImageFilePath.getPath(this, imageUri!!)!!}")

            }
        }
    }

    private fun getImageUri(inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val path = MediaStore.Images.Media.insertImage(
            this.contentResolver,
            inImage,
            "" + System.currentTimeMillis(),
            null
        )
        return Uri.parse(path)
    }

    fun getBitmapFromView(view: View): Bitmap? {
        //Define a bitmap with the same size as the view
        val returnedBitmap = Bitmap.createBitmap(900, 50, Bitmap.Config.ARGB_8888)
        //Bind a canvas to it
        val canvas = Canvas(returnedBitmap)
        //Get the view's background
        val bgDrawable = view.background
        if (bgDrawable != null) //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas) else  //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE)
        // draw the view on the canvas
        view.draw(canvas)
        //return the bitmap
        return returnedBitmap
    }

    var privacyFlag = 0
    private fun privacyClicked() {
        if (privacyFlag == 0) {
            imgPrivacy.setImageResource(R.mipmap.check1)
        } else if (privacyFlag == 1) {
            imgPrivacy.setImageResource(R.mipmap.check2)
        }
        privacyFlag = if (privacyFlag == 0) 1 else 0
    }

    private fun setTopBar() {
        imgBack.visibility = View.VISIBLE
        imgBack.setOnClickListener { finish() }
        txtTitle.text = getString(R.string.book)
    }

    override fun <T> onSuccess(response: T) {

        val coach = intent.getStringExtra("COACH_JOIN").toString()
        val eid = intent.getStringExtra("eventId").toString()

        if (coach == "coach_join") {
            showToast("Book Successful")
            onBackPressed()
            finish()

        } else {
            val intent = Intent(this, EventDetailsActivity::class.java)
            intent.putExtra("eventId", eid)
            startActivity(intent)
            showToast("Book Successful")
            finish()
        }
    }

    override fun onFail(error: String?) {

        showToast(error)

    }

    override fun <T> onSuccessNew(response: T, method: String) {
        TODO("Not yet implemented")
    }
}