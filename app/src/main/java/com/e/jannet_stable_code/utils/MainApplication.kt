package com.e.jannet_stable_code.utils

import android.app.Application
import android.content.Context
import android.util.Log
import org.jetbrains.annotations.Contract
import org.jetbrains.annotations.Nullable


public class MainApplication:Application() {

    public var isAppOpened=0

    private var sInstance: MainApplication? = null

    public fun getAppContext(): MainApplication? {
        return sInstance
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate() called")
        sInstance = this
    }
}